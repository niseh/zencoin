package controllers

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits._

import play.api.libs.ws.WS

import play.api.libs.concurrent._
import play.api.libs.json._
import play.api.libs.iteratee._
import play.api.libs.EventSource

import play.api.mvc._

object RealtimeController extends Controller {
    private lazy val LOGGER = play.api.Logger("controllers.RealtimeController")
    val ( output, channel ) = Concurrent.broadcast[JsValue]

    def realtime = Action {
        Ok.stream(
            (output).map { chunk =>
                LOGGER.info("Sending : " + chunk)
                chunk
            } &> EventSource[JsValue]()
        ).withHeaders("Content-Type" -> "text/event-stream")
    }
}