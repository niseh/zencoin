package controllers

import play.api._
import play.api.mvc._
import play.api.libs.ws.WS
import scala.concurrent.ExecutionContext.Implicits._
import play.api.libs.json._

object Application extends Controller {
  
	def index = Action {
    	Ok(views.html.index("Your new application is ready."))
	}

	def ipToGeolocation(ip: String) = Action {
		Async {
			WS.url("http://freegeoip.net/json/" + ip).get() map { response =>
				val json = Json.parse(response.body)
				val lat = (json \ "latitude").asOpt[Double].getOrElse("")
				val lon = (json \ "longitude").asOpt[Double].getOrElse("")

				val lonlat = Map("lat" -> lat.toString, "lon" -> lon.toString)
				Ok(Json.toJson(lonlat))
			}
		}
	}
}