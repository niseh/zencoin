import play.api._
import play.api.mvc._

import utils.WebSocketClient

import akka.actor.Actor
import akka.actor.Props



object Global extends GlobalSettings{

    override def onStart(app: Application) {
        import play.api.libs.concurrent.Execution.Implicits._

        scala.concurrent.Future{
            val actorRef = play.libs.Akka.system.actorOf(Props[utils.MyActor], name = "myactor")
            utils.WebSocketClient(new java.net.URI("ws://ws.blockchain.info:8335/inv"), actorRef).connect
        }
    }

}