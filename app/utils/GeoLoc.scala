package utils

import scala.concurrent.Future
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._

import scala.concurrent.ExecutionContext.Implicits._

import play.api.libs.ws.WS

object GeoLoc{

    val clean = (
        (__ \ "latitude").json.pickBranch and
        (__ \ "longitude").json.pickBranch
    ).reduce


    def ipToGeolocation(ip: String): Future[Option[JsObject]] = {
        WS.url("http://freegeoip.net/json/" + ip).get() map { response =>
            try{
                Json.parse(response.body).transform(clean).asOpt
            }catch{
                case e: Exception => {
                    play.Logger.error("Failed to parse geoLoc : " + e.getMessage())
                    None
                }
            }
        }
    }
}
