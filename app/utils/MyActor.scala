package utils

import scala.concurrent.Future
import utils.WebSocketClient.Messages._

import scala.concurrent.ExecutionContext.Implicits._

import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._

import akka.actor.Actor
import akka.actor.Props

class MyActor extends Actor {
    import play.api.libs.json._

    def receive = {
        case Connected(client) => {
            play.Logger.debug("Connection has been established to: " + client.url.toASCIIString)
            client send("""{"op":"unconfirmed_sub"}""")
        }
        case Disconnected(client, _) => {
            play.Logger.debug("The websocket to " + client.url.toASCIIString + " disconnected.")

            scala.concurrent.Future{
                val actorRef = play.libs.Akka.system.actorOf(Props[utils.MyActor])
                utils.WebSocketClient(new java.net.URI("ws://ws.blockchain.info:8335/inv"), actorRef).connect
            }
        }
        case TextMessage(client, message) => {
            try{
                val json = Json.parse(message)
                json.transform(MyActor.clean).map({ js =>
                    innerGeoLoc(js).map({ o =>
                        o.map({ geo =>
                            play.Logger.info("Last : - " + geo)
                            controllers.RealtimeController.channel.push(geo)
                        })
                    }).recover({
                        case e: Exception => e.printStackTrace();
                    })
                })
            }catch{
                case e: Exception => {
                    play.Logger.error("Failed to parse  :" + e.getMessage() + "\n" + message)
                }
            }
        }
    }

    def innerGeoLoc( js: JsValue ): Future[Option[JsValue]] = {
        val ip = (js \ "ip").as[String]
        utils.GeoLoc.ipToGeolocation( ip ).map(
            _.map({ loc: JsObject => loc + ("value", (js \ "amount").as[JsNumber]) })
        )
    }
}

object MyActor{
    val clean = (
        (__ \ "ip").json.copyFrom((__ \ "x" \ "relayed_by").json.pick) and
        (__ \ "amount").json.copyFrom(
            (__ \ "x" \ "inputs" ).json.pick[JsArray].map({ js =>
                val sum = js.value.foldLeft(0L)({ (acc, value) =>
                    acc + (value \ "prev_out" \ "value").as[Long]
                })
                JsNumber(sum)
            })
        )
    ).reduce
}